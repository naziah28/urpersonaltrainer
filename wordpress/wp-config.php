<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'urpersonaltrainer');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eq)IQiwttPsG ?*:;uM{3/Lqvb+77k7es1%n]$hfJee CbkQ*qHtDD=aw3Wh)3Ss');
define('SECURE_AUTH_KEY',  '7Bj6l1BEA+nZtRzU2Vd~![ChkXBluCkvuZ%}=unt!&J.Iy*{M^+C!pPj`P ;OLdu');
define('LOGGED_IN_KEY',    'OxNeSA#%4B0<G-nQt7[p^>8wXM53N<6U|S$J[6nd`W@Q;sS.z3hm,,26E:  jGo=');
define('NONCE_KEY',        '1Lig:hwvu^h?u~C=0xvWVNfmL4#y+#g<Gb^HZg;)W7j%CX)Dijh.#N(Ez:5Y[NlA');
define('AUTH_SALT',        '?pny^?l^]3vL`<V=s}^h+4b>EfZ|x3A[$ZKU{R}/B%6}>#YhL=jXXs(05eI}{;iy');
define('SECURE_AUTH_SALT', 'RA`s^hd>Y/`OFPX2C99SWM4s-0f`!*tDJ7j^7~Z_eG>9 I )(r0T.3ik|zV]?3A8');
define('LOGGED_IN_SALT',   '_*iBywR=Rb2ayT}iRh(d(WN%;i?p[!|TqtS1&Z8^*,+o_,!DH8A&uInmg!7sJA+y');
define('NONCE_SALT',       ':Uob!Do >1WEByt@zS&a7I,u5ex25?#%^t/:@ )QZXQVf #Eb~mULwh_;X~Y/crX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
