<?php
/**
 * VW Fitness functions and definitions
 *
 * @package VW Fitness
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */

function vw_fitness_the_breadcrumb() {
	if (!is_home()) {
		echo '<a href="';
			echo esc_url( home_url() );
		echo '">';
			bloginfo('name');
		echo "</a> ";
		if (is_category() || is_single()) {
			the_category(',');
			if (is_single()) {
				echo "<span> ";
					the_title();
				echo "</span> ";
			}
		} elseif (is_page()) {
			the_title();
		}
	}
}

if ( ! function_exists( 'vw_fitness_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function vw_fitness_setup() {

	$GLOBALS['content_width'] = apply_filters( 'vw_fitness_content_width', 640 );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );
	add_image_size('vw-fitness-homepage-thumb',240,145,true);
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'vw-fitness' ),
		'footer'	=> __('Footer Menu', 'vw-fitness'),
	) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'f1f1f1'
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', vw_fitness_font_url() ) );

}
endif; // vw_fitness_setup
add_action( 'after_setup_theme', 'vw_fitness_setup' );


function vw_fitness_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'vw-fitness' ),
		'description'   => __( 'Appears on blog page sidebar', 'vw-fitness' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Page Sidebar', 'vw-fitness' ),
		'description'   => __( 'Appears on page sidebar', 'vw-fitness' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Third Column Sidebar', 'vw-fitness' ),
		'description'   => __( 'Appears on page sidebar', 'vw-fitness' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'vw-fitness' ),
		'description'   => __( 'Appears on page sidebar', 'vw-fitness' ),
		'id'            => 'footer-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'vw-fitness' ),
		'description'   => __( 'Appears on page sidebar', 'vw-fitness' ),
		'id'            => 'footer-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 3', 'vw-fitness' ),
		'description'   => __( 'Appears on page sidebar', 'vw-fitness' ),
		'id'            => 'footer-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 4', 'vw-fitness' ),
		'description'   => __( 'Appears on page sidebar', 'vw-fitness' ),
		'id'            => 'footer-4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'vw_fitness_widgets_init' );


function vw_fitness_font_url(){
		$font_url = '';
		
		/* Translators: If there are any character that are
		* not supported by PT Sans, translate this to off, do not
		* translate into your own language.
		*/
		$ptsans = _x('on', 'PT Sans font:on or off','vw-fitness');
		
		/* Translators: If there are any character that are
		* not supported by Roboto, translate this to off, do not
		* translate into your own language.
		*/
		$roboto = _x('on', 'Roboto font:on or off','vw-fitness');
		
		/* Translators: If there are any character that are
		* not supported by Roboto Condensed, translate this to off, do not
		* translate into your own language.
		*/
		$roboto_cond = _x('on', 'Roboto Condensed font:on or off','vw-fitness');

		/* Translators: If there are any character that are
		* not supported by PT Sans, translate this to off, do not
		* translate into your own language.
		*/
		$oxygen = _x('on', 'Oxygen font:on or off','vw-fitness');

		/* Translators: If there are any character that are
		* not supported by PT Sans, translate this to off, do not
		* translate into your own language.
		*/
		$Open_sans = _x('on', '$Open sans font:on or off','vw-fitness');
		
		if('off' !== $ptsans || 'off' !==  $roboto || 'off' !== $roboto_cond){
			$font_family = array();
			
			if('off' !== $ptsans){
				$font_family[] = 'PT Sans:300,400,600,700,800,900';
			}
			
			if('off' !== $roboto){
				$font_family[] = 'Roboto:400,700';
			}
			
			if('off' !== $roboto_cond){
				$font_family[] = 'Roboto Condensed:400,700';
			}

			if('off' !== $oxygen){
				$font_family[] = 'Oxygen:400,700';
			}

			if('off' !== $Open_sans){
				$font_family[] = 'Open Sans:300,400,600,700,800,900';
			}
			
			$query_args = array(
				'family'	=> urlencode(implode('|',$font_family)),
			);
			
			$font_url = add_query_arg($query_args,'//fonts.googleapis.com/css');
		}
	return $font_url;
}
	
function vw_fitness_scripts() {
	
	wp_enqueue_style( 'vw-fitness-font', vw_fitness_font_url(), array() );
	wp_enqueue_style( 'vw-fitness-style', get_stylesheet_uri() );
	wp_style_add_data( 'vw-fitness-style', 'rtl', 'replace' );
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri().'/css/bootstrap.css' );
	wp_enqueue_style( 'effect', get_template_directory_uri().'/css/effect.css' );
	wp_enqueue_style( 'vw-fitness-customcss', get_template_directory_uri() . '/css/custom.css' );
	if ( is_home() || is_front_page() ) { 
		wp_enqueue_style( 'nivo-style', get_template_directory_uri().'/css/nivo-slider.css' );
		wp_enqueue_script( 'nivo-slider', get_template_directory_uri() . '/js/jquery.nivo.slider.js', array('jquery') );
	}
	wp_enqueue_script( 'tether', get_template_directory_uri() . '/js/tether.js', array('jquery') ,'',true);
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery') ,'',true);
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/jquery-3.2.1.js', array('jquery') ,'',true);
	wp_enqueue_script( 'vw-fitness-customscripts', get_template_directory_uri() . '/js/custom.js', array('jquery') );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_style('vw-fitness-ie', get_template_directory_uri().'/css/ie.css', array('vw-fitness-ie-style'));
	wp_style_add_data( 'vw-fitness-ie', 'conditional', 'IE' );
}
add_action( 'wp_enqueue_scripts', 'vw_fitness_scripts' );

function vw_fitness_ie_stylesheet() {
	wp_enqueue_style('vw-fitness-ie', get_template_directory_uri().'/css/ie.css', array('vw-fitness-ie-style'));
	wp_style_add_data( 'vw-fitness-ie', 'conditional', 'IE' );
}
add_action('wp_enqueue_scripts','vw_fitness_ie_stylesheet');

define('vw_fitness_CREDIT','https://www.vwthemes.com','vw-fitness');

if ( ! function_exists( 'vw_fitness_credit' ) ) {
	function vw_fitness_credit(){
			echo "<a href=".esc_url(vw_fitness_CREDIT)." target='_blank'>VWThemes</a>";
	}
}

define('vw_fitness_CREDIT1','https://www.vwthemes.com/premium/gym-fitness-wordpress-theme/','vw-fitness');

if ( ! function_exists( 'vw_fitness_credit1' ) ) {
	function vw_fitness_credit1(){
			echo "<a href=".esc_url(vw_fitness_CREDIT1)." target='_blank'>Fitness WordPress Theme</a>";
	}
}

/*radio button sanitization*/

 function vw_fitness_sanitize_choices( $input, $setting ) {

    global $wp_customize; 

    $control = $wp_customize->get_control( $setting->id ); 

    if ( array_key_exists( $input, $control->choices ) ) {

        return $input;

    } else {

        return $setting->default;

    }
}

/* Mailing Function */
if ( is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
	// do your stuff
	$url = get_site_url();
	// The message
	$message = "A Free VW Fitness Theme is activated on $url ";

	// In case any of our lines are larger than 70 characters, we should use wordwrap()
	$message = wordwrap($message, 70, "\r\n");

	// Send
wp_mail('info@vwthemes.com', 'Free Fitness Theme Activated', $message);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/* Implement the Custom Header feature. */
require get_template_directory() . '/inc/custom-header.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
