<?php
/**
 * VW Fitness Theme Customizer
 *
 * @package VW Fitness
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function vw_fitness_customize_register( $wp_customize ) {

	//add home page setting pannel
	$wp_customize->add_panel( 'vw_fitness_panel_id', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'VW Settings', 'vw-fitness' ),
	    'description' => __( 'Description of what this panel does.', 'vw-fitness' ),
	) );

	//Layouts
	$wp_customize->add_section( 'vw_fitness_left_right', array(
    	'title'      => __( 'Theme Layout Settings', 'vw-fitness' ),
		'priority'   => 30,
		'panel' => 'vw_fitness_panel_id'
	) );

	// Add Settings and Controls for Layout
	$wp_customize->add_setting('vw_fitness_theme_options',array(
	        'default' => '',
	        'sanitize_callback' => 'vw_fitness_sanitize_choices'
	    )
    );

	$wp_customize->add_control('vw_fitness_theme_options',
	    array(
	        'type' => 'radio',
	        'label' => 'Do you want this section',
	        'section' => 'vw_fitness_left_right',
	        'choices' => array(
	            'Left Sidebar' => __('Left Sidebar','vw-fitness'),
	            'Right Sidebar' => __('Right Sidebar','vw-fitness'),
	            'One Column' => __('One Column','vw-fitness'),
	            'Three Columns' => __('Three Columns','vw-fitness'),
	            'Four Columns' => __('Four Columns','vw-fitness'),
	            'Grid Layout' => __('Grid Layout','vw-fitness')
	        ),
	    )
    );	

	//home page slider
	$wp_customize->add_section( 'vw_fitness_slidersettings' , array(
    	'title'      => __( 'Slider Settings', 'vw-fitness' ),
		'priority'   => 30,
		'panel' => 'vw_fitness_panel_id'
	) );

	for ( $count = 1; $count <= 3; $count++ ) {

		// Add color scheme setting and control.
		$wp_customize->add_setting( 'vw_fitness_slidersettings-page-' . $count, array(
				'default'           => '',
				'sanitize_callback' => 'absint'
		) );

		$wp_customize->add_control( 'vw_fitness_slidersettings-page-' . $count, array(
			'label'    => __( 'Select Slide Image Page', 'vw-fitness' ),
			'section'  => 'vw_fitness_slidersettings',
			'type'     => 'dropdown-pages'
		) );

	}

	//OUR services
	$wp_customize->add_section('vw_fitness_our_services',array(
		'title'	=> __('Our Services','vw-fitness'),
		'description'=> __('This section will appear below the slider.','vw-fitness'),
		'panel' => 'vw_fitness_panel_id',
	));	

	for ( $count = 0; $count <= 3; $count++ ) {

		$wp_customize->add_setting( 'vw_fitness_servicesettings-page-' . $count, array(
			'default'           => '',
			'sanitize_callback' => 'absint'
		));
		$wp_customize->add_control( 'vw_fitness_servicesettings-page-' . $count, array(
			'label'    => __( 'Select Service Page', 'vw-fitness' ),
			'section'  => 'vw_fitness_our_services',
			'type'     => 'dropdown-pages'
		));
	}
	
}
add_action( 'customize_register', 'vw_fitness_customize_register' );


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function vw_fitness_customize_preview_js() {
	wp_enqueue_script( 'vw_fitness_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'vw_fitness_customize_preview_js' );

/**
 * Singleton class for handling the theme's customizer integration.
 *
 * @since  1.0.0
 * @access public
 */
final class vw_fitness_customize {

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Sets up initial actions.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup_actions() {

		// Register panels, sections, settings, controls, and partials.
		add_action( 'customize_register', array( $this, 'sections' ) );

		// Register scripts and styles for the controls.
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_control_scripts' ), 0 );
	}

	/**
	 * Sets up the customizer sections.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  object  $manager
	 * @return void
	 */
	public function sections( $manager ) {

		// Load custom sections.
		load_template( trailingslashit( get_template_directory() ) . '/inc/section-pro.php' );

		// Register custom section types.
		$manager->register_section_type( 'vw_fitness_customize_Section_Pro' );

		// Register sections.
		$manager->add_section(
			new vw_fitness_customize_Section_Pro(
				$manager,
				'example_1',
				array(
					'title'    => esc_html__( 'VW Fitness Pro Theme', 'vw-fitness' ),
					'pro_text' => esc_html__( 'Go Pro', 'vw-fitness' ),
					'pro_url'  => 'https://www.vwthemes.com/premium/gym-fitness-wordpress-theme/'
				)
			)
		);
	}

	/**
	 * Loads theme customizer CSS.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function enqueue_control_scripts() {

		wp_enqueue_script( 'vw-fitness-customize-controls', trailingslashit( get_template_directory_uri() ) . '/js/customize-controls.js', array( 'customize-controls' ) );

		wp_enqueue_style( 'vw-fitness-customize-controls', trailingslashit( get_template_directory_uri() ) . '/css/customize-controls.css' );
	}
}

// Doing this customizer thang!
vw_fitness_customize::get_instance();